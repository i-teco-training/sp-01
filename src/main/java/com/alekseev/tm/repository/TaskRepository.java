package com.alekseev.tm.repository;

import com.alekseev.tm.entity.Task;
import org.springframework.stereotype.Repository;

@Repository
public final class TaskRepository extends AbstractRepository<Task> {
    public TaskRepository() {}
}
