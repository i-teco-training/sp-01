package com.alekseev.tm.repository;

import com.alekseev.tm.api.IRepository;
import com.alekseev.tm.entity.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {
    private HashMap<String, E> entityMap = new HashMap<>();

    @Override
    public void addAll(List<E> entityList) {
        for (E entity : entityList) {
            entityMap.put(entity.getId(), entity);
        }
    }

    @Override
    @Nullable
    public E findOne(String id) {
        return entityMap.get(id);
    }

    @Override
    @NotNull
    public List<E> findAll() {
        List<E> entities = new ArrayList<>(entityMap.values());
        return entities;
    }

    @Override
    public void persist(E entity) {
        if (!entityMap.containsKey(entity.getId()))
            entityMap.put(entity.getId(), entity);
    }

    @Override
    public void merge(E entity) {
        entityMap.put(entity.getId(), entity);
    }

    @Override
    public void deleteOne(String id) {
        entityMap.remove(id);
    }

    @Override
    public void deleteAll() {
        entityMap.clear();
    }
}
