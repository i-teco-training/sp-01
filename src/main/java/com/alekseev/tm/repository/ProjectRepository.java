package com.alekseev.tm.repository;

import com.alekseev.tm.entity.Project;
import org.springframework.stereotype.Repository;

@Repository
public final class ProjectRepository extends AbstractRepository<Project> {
    public ProjectRepository() {}
}
