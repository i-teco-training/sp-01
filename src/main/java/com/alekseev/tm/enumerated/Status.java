package com.alekseev.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum Status {
    PLANNED("planned"),
    INPROGRESS("in progress"),
    READY("ready");

    Status() {}

    private String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

    @Override
    @Nullable
    public final String toString() {
        return this.displayName;
    }

    @NotNull
    public static Status getStatusFromString(@Nullable final String displayName){
        for (@NotNull final Status status: Status.values()) {
            if (status.toString().equals(displayName))
                return status;
        }
        return PLANNED;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
