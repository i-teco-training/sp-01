package com.alekseev.tm.service;

import com.alekseev.tm.api.IService;
import com.alekseev.tm.entity.AbstractEntity;
import com.alekseev.tm.repository.AbstractRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class AbstractService<E extends AbstractEntity> implements IService<E> {
    @Autowired
    public AbstractRepository<E> repository;

    @Override
    public void addOne(E entity) {
        repository.persist(entity);
    }

    @Override
    public void addAll(List<E> entityList) {
        for (E e : entityList)
            repository.persist(e);
    }

    @Override
    public E findOne(String id) {
        return repository.findOne(id);
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public void update(E entity) {
        repository.merge(entity);
    }

    @Override
    public void deleteOne(String id) {
        repository.deleteOne(id);
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }
}
