package com.alekseev.tm.service;

import com.alekseev.tm.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

@Service
public final class ProjectService extends AbstractService<Project> {

    public final void initProjects() {
        if (repository.findOne("001") == null) {
            @NotNull final Project project = new Project();
            project.setId("001");
            project.setName("football");
            project.setDescription("first project");
            @NotNull final Project project2 = new Project();
            project2.setId("002");
            project2.setName("baseball");
            project2.setDescription("second project");
            repository.persist(project);
            repository.persist(project2);
        }
    }
}
