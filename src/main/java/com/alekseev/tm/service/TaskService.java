package com.alekseev.tm.service;

import com.alekseev.tm.entity.Task;
import org.springframework.stereotype.Service;

@Service
public final class TaskService extends AbstractService<Task> {
}
