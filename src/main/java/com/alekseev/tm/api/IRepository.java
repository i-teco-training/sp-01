package com.alekseev.tm.api;

import com.alekseev.tm.entity.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void addAll(List<E> entityList);

    E findOne(String id);

    List<E> findAll();

    void persist(E entity);

    void merge(E entity);

    void deleteOne(String id);

    void deleteAll();
}
