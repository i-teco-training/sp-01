package com.alekseev.tm.api;

import com.alekseev.tm.entity.AbstractEntity;

import java.util.List;

public interface IService<E extends AbstractEntity> {

    void addOne(E entity);

    void addAll(List<E> entityList);

    E findOne(String id);

    List<E> findAll();

    void update(E entity);

    void deleteOne(String id);

    void deleteAll();
}
