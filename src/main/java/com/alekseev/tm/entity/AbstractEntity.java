package com.alekseev.tm.entity;

import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public class AbstractEntity {
    public AbstractEntity() {
    }

    @NotNull
    private String id = UUID.randomUUID().toString();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
