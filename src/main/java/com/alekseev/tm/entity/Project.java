package com.alekseev.tm.entity;

import com.alekseev.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

//@Entity
public class Project extends AbstractEntity {
    public Project() {
    }

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    //@DateTimeFormat(pattern = "MM-dd-yyyy")
    private Date createdOn = new Date();

    @Nullable
    public String getName() {
        return name;
    }


    public void setName(@Nullable String name) {
        this.name = name;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    @Nullable
    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(@Nullable Date dateStart) {
        this.dateStart = dateStart;
    }

    @Nullable
    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(@Nullable Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    @NotNull
    public Status getStatus() {
        return status;
    }

    public void setStatus(@NotNull Status status) {
        this.status = status;
    }

    @Nullable
    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(@Nullable Date createdOn) {
        this.createdOn = createdOn;
    }
}
