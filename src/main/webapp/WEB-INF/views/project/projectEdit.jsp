<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item"><a href="/project">Projects</a></li>
        <li class="breadcrumb-item active" aria-current="page">Add Project</li>
      </ol>
    </nav>
<style>
        #create{
            margin-top:20px;
            margin-left:50px;
        }
        #form{
            margin-left:50px;
            width: 50%;
        }
        #status{
            margin-left:50px;
            width: 50%;
        }
        #button{
            margin-top:20px;
            margin-left:50px;
        }
        #button2{
            margin-top:20px;
        }
 </style>

<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <c:if test="${empty project.id}">
        <title>[Add project]</title>
    </c:if>
    <c:if test="${!empty project.id}">
        <title>[Update project]</title>
    </c:if>
</head>
<body>
<c:if test="${empty project.id}">
    <c:url value="/add-project" var="var"/>
</c:if>
<c:if test="${!empty project.id}">
    <c:url value="/edit-project" var="var"/>
</c:if>
<form action="${var}" method="POST">
    <c:if test="${!empty project.id}">
        <input type="hidden" name="id" value="${project.id}">
    </c:if>
        <div class="form-group">
             <label for="name">Project name</label>
             <input type="text" name="name" id="name">
        </div>
        <div class="form-group">
             <label for="description">Project description</label>
             <input type="text" name="description" id="description">
        </div>
        <div class="form-group">
             <label for="createdOn">Project creation date</label>
             <input type="date" name="createdOn" id="createdOn">
        </div>

    <c:if test="${empty project.id}">
        <input id="button" class="btn btn-outline-success" type="submit" value="Add new project">
    </c:if>
    <c:if test="${!empty project.id}">
        <input id="button" class="btn btn-outline-success" type="submit" value="Update project">
    </c:if>

    <c:url value="/project" var="project"/>
    <a id="button2" href="${project}" class="btn btn-outline-danger">Cancel</a>

</form>
</body>
</html>